# SudoQu

Project that aims to create a Sudoku game implementation in C++ using Qt5, thus the name. It is created as a project for
Software Engineering course at the Cracow University of Technology.

## Prerequisites

Project is based on C++14 and Qt5 library (GUI created with Qt Widgets). Tests writen in the Boost Test library.

## Getting Started

It is recommended to use **CMake** with included `CMakeLists.txt` file. To build in a `build` directory:

```
git clone git@gitlab.com:Koci0/SudoQu.git
cd SudoQu
mkdir build
cd build
cmake ..
make
```

## Authors

* **Tomasz Kot** - *Initial work* - [Koci0](https://github.com/Koci0)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

