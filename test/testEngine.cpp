#pragma clang diagnostic push
#pragma ide diagnostic ignored "altera-struct-pack-align"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Main

#include <boost/test/unit_test.hpp>

#include "engine.h"

struct EngineFixture {
    Engine *engine;

    EngineFixture() : engine(new Engine()) {}

    ~EngineFixture() = default;
};

struct EngineCorrectFixture {
    Engine *engine;

    EngineCorrectFixture() : engine(new Engine()) {
        QVector<int> correctSudoku = {
                1, 8, 6, 2, 5, 7, 4, 9, 3,
                2, 9, 7, 4, 3, 6, 5, 8, 1,
                5, 3, 4, 9, 1, 8, 2, 7, 6,
                7, 1, 3, 8, 2, 5, 9, 6, 4,
                4, 6, 8, 3, 9, 1, 7, 2, 5,
                9, 2, 5, 7, 6, 4, 3, 1, 8,
                6, 4, 1, 5, 7, 9, 8, 3, 2,
                3, 5, 9, 6, 8, 2, 1, 4, 7,
                8, 7, 2, 1, 4, 3, 6, 5, 9
        };

        for (int i = 0; i < engine->getBoard()->size(); i++) {
            engine->updateIndex(i, correctSudoku.at(i));
        }
    }

    ~EngineCorrectFixture() = default;
};

BOOST_AUTO_TEST_SUITE(testIsBoardCorrect);

    BOOST_FIXTURE_TEST_CASE(positive, EngineCorrectFixture)
    {
        BOOST_CHECK(engine->isBoardCorrect() == true);
    }

    BOOST_AUTO_TEST_SUITE(negative);

        BOOST_FIXTURE_TEST_CASE(all, EngineFixture)
        {
            for (int row = 0; row < SUDOKU_SIZE; row++) {
                for (int column = 0; column < SUDOKU_SIZE; column++) {
                    engine->updateIndex(row * SUDOKU_SIZE + column, column + 1);
                }
            }

            BOOST_CHECK(engine->isBoardCorrect() == false);
        }

        BOOST_FIXTURE_TEST_CASE(row, EngineCorrectFixture)
        {
            engine->updateIndex(0, engine->getBoard()->at(SUDOKU_SIZE - 1));

            BOOST_CHECK(engine->isBoardCorrect() == false);
        }

        BOOST_FIXTURE_TEST_CASE(column, EngineCorrectFixture)
        {
            engine->updateIndex(0, engine->getBoard()->at(SUDOKU_SIZE * (SUDOKU_SIZE - 1)));

            BOOST_CHECK(engine->isBoardCorrect() == false);
        }

        BOOST_FIXTURE_TEST_CASE(square, EngineCorrectFixture)
        {
            engine->updateIndex(0, engine->getBoard()->at(2));

            BOOST_CHECK(engine->isBoardCorrect() == false);
        }

    BOOST_AUTO_TEST_SUITE_END();

    BOOST_FIXTURE_TEST_CASE(empty, EngineFixture)
    {
        BOOST_CHECK(engine->isBoardCorrect() == true);
    }

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(testIsBoardFilled);

    BOOST_FIXTURE_TEST_CASE(positive, EngineCorrectFixture)
    {
        BOOST_CHECK(engine->isBoardFilled() == true);
    }

    BOOST_FIXTURE_TEST_CASE(negativeNotFilled, EngineCorrectFixture)
    {
        engine->updateIndex(0, 0);
        BOOST_CHECK(engine->isBoardFilled() == false);
    }

    BOOST_FIXTURE_TEST_CASE(negativeEmpty, EngineFixture)
    {
        BOOST_CHECK(engine->isBoardFilled() == false);
    }

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(testGenerateNewBoard);

    BOOST_FIXTURE_TEST_CASE(fullBoard, EngineFixture)
    {
        engine->generateNewBoard(0);
        BOOST_CHECK(engine->isBoardFilled() == true);
        BOOST_CHECK(engine->isBoardCorrect() == true);
    }

    BOOST_FIXTURE_TEST_CASE(boardWithEmptyNumbers, EngineFixture)
    {
        engine->generateNewBoard(1);
        BOOST_CHECK(engine->isBoardFilled() == false);
        BOOST_CHECK(engine->isBoardCorrect() == true);
    }

BOOST_AUTO_TEST_SUITE_END();
