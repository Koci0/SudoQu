#include "boardbutton.h"

BoardButton::BoardButton(int row, int column) : QPushButton() {
    this->row = row;
    this->column = column;
    this->index = row * SUDOKU_SIZE + column;
}

int BoardButton::getRow() const {
    return row;
}

int BoardButton::getColumn() const {
    return column;
}

int BoardButton::getIndex() const {
    return index;
}
