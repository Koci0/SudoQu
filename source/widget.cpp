#include "widget.h"

Widget::Widget(QWidget *parent)
        : QWidget(parent) {
    auto mainLayout = new QVBoxLayout;

    boardLayout = new QGridLayout;
    setupSquareLayouts();
    setupBoardButtons();
    setupStatusLayout();

    mainLayout->addLayout(boardLayout);
    mainLayout->addLayout(statusLayout);

    setLayout(mainLayout);
    setWindowTitle("SudoQu");

    engine = new Engine;
    connect(this, &Widget::boardChanged, this, &Widget::onBoardChange);

    selectedBoardButton = nullptr;
}

void Widget::setupSquareLayouts() {
    squareLayouts = new QVector<QGridLayout *>;
    for (int i = 0; i < SUDOKU_SIZE; i++) {
        auto tmpSquareLayout = new QGridLayout;
        tmpSquareLayout->setMargin(LAYOUT_MARGIN);
        squareLayouts->append(tmpSquareLayout);
        boardLayout->addLayout(tmpSquareLayout, i / SQUARE_SIZE, i % SQUARE_SIZE);
    }
}

void Widget::setupBoardButtons() {
    boardButtons = new QVector<BoardButton *>;
    for (int row = 0; row < SUDOKU_SIZE; row++) {
        for (int column = 0; column < SUDOKU_SIZE; column++) {
            auto tmpBoardButton = new BoardButton(row, column);
            tmpBoardButton->setFixedSize(BUTTON_SIZE, BUTTON_SIZE);
            boardButtons->append(tmpBoardButton);
            connect(tmpBoardButton, &QPushButton::clicked, this, &Widget::onBoardButtonClick);
        }
    }

    for (auto &button : *boardButtons) {
        int squareIndex = (button->getRow() / SQUARE_SIZE) * SQUARE_SIZE + (button->getColumn() / SQUARE_SIZE);
        squareLayouts->at(squareIndex)->addWidget(button, button->getRow(), button->getColumn());
    }

    disableBoardButtons();
}

void Widget::setupStatusLayout() {
    statusLayout = new QHBoxLayout;

    newButton = new QPushButton("New Board");
    connect(newButton, &QPushButton::clicked, this, &Widget::onNewButtonClick);

    statusLabel = new QLabel();
    statusLabel->setAlignment(Qt::AlignCenter);

    undoButton = new QPushButton("Undo");
    connect(undoButton, &QPushButton::clicked, this, &Widget::onUndoButtonClicked);

    statusLayout->addWidget(newButton);
    statusLayout->addWidget(statusLabel);
    statusLayout->addWidget(undoButton);
}

void Widget::fillBoardWithNumbers(QVector<int> *numbers) {
    if (boardButtons->size() != numbers->size()) {
        throw;
    }
    for (int i = 0; i < boardButtons->size(); i++) {
        if (numbers->at(i) != 0) {
            boardButtons->at(i)->setText(QString::number(numbers->at(i)));
        } else {
            boardButtons->at(i)->setText("");
        }
    }
}

void Widget::selectBoardButton(BoardButton *button) {
    if (selectedBoardButton) {
        selectedBoardButton->setStyleSheet("background-color: #ffffff");
    }

    selectedBoardButton = button;
    selectedBoardButton->setStyleSheet("background-color:  #cdcdcd");
}

void Widget::enableBoardButtons() {
    for (auto &button : *boardButtons) {
        button->setEnabled(true);
    }
}

void Widget::disableBoardButtons() {
    for (auto &button : *boardButtons) {
        button->setDisabled(true);
    }
}

void boardChanged() {}

void Widget::onBoardButtonClick() {
    selectBoardButton((BoardButton *) qobject_cast<QPushButton *>(sender()));
}


void Widget::onNewButtonClick() {
    engine->queue->reset();
    engine->generateNewBoard(EMPTY_NUMBERS);
    fillBoardWithNumbers(engine->getBoard());
    statusLabel->setText("NOT SOLVED");
    enableBoardButtons();
    this->undoButton->setEnabled(true);
}

void Widget::onUndoButtonClicked() {
    auto lastMovement = engine->queue->getLastMovement();
    qDebug() << lastMovement;
    if (lastMovement) {
        int index = lastMovement->first;
        int value = lastMovement->second;
        boardButtons->at(index)->setText(value ? QString::number(value) : "");
        engine->updateIndex(index, value);
    }
}

void Widget::keyPressEvent(QKeyEvent *event) {
    auto keyPressed = event->key();

    if (selectedBoardButton) {
        if (keyPressed == Qt::Key_Escape) {
            selectedBoardButton->setStyleSheet("");
            selectedBoardButton = nullptr;
        } else if (keyPressed == Qt::Key_0) {
            int previousValue = selectedBoardButton->text().toInt();
            engine->queue->addMovement(selectedBoardButton->getIndex(), previousValue);
            selectedBoardButton->setText("");
            selectedBoardButton->setStyleSheet("");
        } else if (keyPressed >= Qt::Key_1 and keyPressed <= Qt::Key_9) {
            int previousValue = selectedBoardButton->text().toInt();
            engine->queue->addMovement(selectedBoardButton->getIndex(), previousValue);
            selectedBoardButton->setText((QString) keyPressed);
            selectedBoardButton->setStyleSheet("");
        }
    }

    emit boardChanged();
}

void Widget::onBoardChange() {
    if (selectedBoardButton) {
        engine->updateIndex(selectedBoardButton->getIndex(), selectedBoardButton->text().toInt());
        if (engine->isBoardCorrect()) {
            if (engine->isBoardFilled()) {
                statusLabel->setText("SOLVED");
                disableBoardButtons();
                this->undoButton->setDisabled(true);
            }
        }
    }
}
