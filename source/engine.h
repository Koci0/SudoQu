#ifndef ENGINE_H
#define ENGINE_H

#include <algorithm>
#include <QVector>
#include <QRandomGenerator>

#include "constants.h"
#include "movementQueue.h"

class Engine {
public:
    MovementQueue *queue;

    Engine();

    void generateNewBoard(int emptyNumbers);

    QVector<int> *getBoard();

    void updateIndex(int index, int value);

    bool isBoardCorrect();

    bool isBoardFilled();

private:
    QVector<int> *board;
    QVector<bool> *filled;

    void fillDiagonalsRandomly();

    bool fillBoardRandomly();

    void clearBoard();

    void incrementBoardIndex(int index);

    void backtrackIndex(int &index);

    void removeRandomNumbers(int n);

    bool areSquaresCorrect();

    bool areRowsCorrect();

    bool areColumnsCorrect();

    bool isBoardIndexFilled(int index);

    static int getRandomNumber(int from, int to);

    static bool isVectorUnique(QVector<int> &vector);
};

#endif // ENGINE_H
