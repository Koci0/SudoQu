#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <QtDebug>

const int SQUARE_SIZE = 3;
const int SUDOKU_SIZE = 9;

#endif // CONSTANTS_H
