#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include <QKeyEvent>

#include <QtMath>

#include "boardbutton.h"
#include "constants.h"
#include "engine.h"

class Widget : public QWidget {
Q_OBJECT
    int LAYOUT_MARGIN = 10;
    int BUTTON_SIZE = 30;

    int EMPTY_NUMBERS = 1;

public:
    explicit Widget(QWidget *parent = nullptr);

private:
    QGridLayout *boardLayout{};
    QVector<QGridLayout *> *squareLayouts{};
    QVector<BoardButton *> *boardButtons{};
    QHBoxLayout *statusLayout{};
    QPushButton *newButton{};
    QLabel *statusLabel{};
    QPushButton *undoButton{};

    Engine *engine;

    BoardButton *selectedBoardButton;

    void setupSquareLayouts();

    void setupBoardButtons();

    void setupStatusLayout();

    void fillBoardWithNumbers(QVector<int> *numbers);

    void selectBoardButton(BoardButton *button);

    void enableBoardButtons();

    void disableBoardButtons();

signals:

    void boardChanged();

private slots:

    void onBoardButtonClick();

    void onNewButtonClick();

    void onUndoButtonClicked();

    void keyPressEvent(QKeyEvent *event) override;

    void onBoardChange();
};

#endif // WIDGET_H
