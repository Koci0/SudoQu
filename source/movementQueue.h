#ifndef APPLICATION_MOVEMENTQUEUE_H
#define APPLICATION_MOVEMENTQUEUE_H

#include <QMap>

class MovementQueue {
    QMap<int, int> *queue;

public:
    MovementQueue();

    void addMovement(int index, int previousNumber);

    QPair<int, int> *getLastMovement();

    void reset();
};

#endif //APPLICATION_MOVEMENTQUEUE_H
