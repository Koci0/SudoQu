#include "movementQueue.h"

MovementQueue::MovementQueue() {
    queue = new QMap<int, int>;
}

void MovementQueue::addMovement(int index, int previousNumber) {
    queue->insert(index, previousNumber);
}

QPair<int, int> *MovementQueue::getLastMovement() {
    if (not queue->empty()) {
        QPair<int, int> *lastMovement = new QPair<int, int>(queue->lastKey(), queue->last());
        queue->remove(queue->lastKey());
        return lastMovement;
    } else {
        return nullptr;
    }
}

void MovementQueue::reset() {
    queue->clear();
}
