#include "engine.h"

Engine::Engine() {
    board = new QVector<int>(SUDOKU_SIZE * SUDOKU_SIZE, 0);
    filled = new QVector<bool>(SUDOKU_SIZE * SUDOKU_SIZE, false);
    queue = new MovementQueue;
}

void Engine::generateNewBoard(int emptyNumbers) {
    bool generatedBoard;
    do {
        clearBoard();
        fillDiagonalsRandomly();
        generatedBoard = fillBoardRandomly();
    } while (not generatedBoard);

    removeRandomNumbers(emptyNumbers);
}

QVector<int> *Engine::getBoard() {
    return board;
}

void Engine::updateIndex(int index, int value) {
    board->replace(index, value);
}

bool Engine::isBoardCorrect() {
    if (not areSquaresCorrect()) {
        return false;
    }
    if (not areRowsCorrect()) {
        return false;
    }
    if (not areColumnsCorrect()) {
        return false;
    }

    return true;
}

bool Engine::isBoardFilled() {
    for (auto &e: *board) {
        if (e == 0) {
            return false;
        }
    }
    return true;
}

void Engine::fillDiagonalsRandomly() {
    do {
        for (int i = 0; i < SUDOKU_SIZE / 2; i++) {
            QVector<int> numbers;
            for (int numberCount = 0; numberCount < 4; numberCount++) {
                int number;
                do {
                    number = getRandomNumber(1, SUDOKU_SIZE);
                } while (numbers.contains(number));
                numbers.append(number);
            }
            board->replace(i * SUDOKU_SIZE + i, numbers.at(0));
            filled->replace(i * SUDOKU_SIZE + i, true);

            board->replace((SUDOKU_SIZE - i - 1) * SUDOKU_SIZE + i, numbers.at(1));
            filled->replace((SUDOKU_SIZE - i - 1) * SUDOKU_SIZE + i, true);

            board->replace(i * SUDOKU_SIZE + (SUDOKU_SIZE - i - 1), numbers.at(2));
            filled->replace(i * SUDOKU_SIZE + (SUDOKU_SIZE - i - 1), true);

            board->replace((SUDOKU_SIZE - i - 1) * SUDOKU_SIZE + (SUDOKU_SIZE - i - 1), numbers.at(3));
            filled->replace((SUDOKU_SIZE - i - 1) * SUDOKU_SIZE + (SUDOKU_SIZE - i - 1), true);
        }

        board->replace(SUDOKU_SIZE / 2 * SUDOKU_SIZE + SUDOKU_SIZE / 2, getRandomNumber(1, SUDOKU_SIZE));
        filled->replace(SUDOKU_SIZE / 2 * SUDOKU_SIZE + SUDOKU_SIZE / 2, true);
    } while (not isBoardCorrect());
}

bool Engine::fillBoardRandomly() {
    int index = 0;
    while (index < board->size()) {
        bool solved = false;

        if (isBoardIndexFilled(index)) {
            index++;
            continue;
        }

        while (board->at(index) < 9) {
            incrementBoardIndex(index);
            if (isBoardCorrect()) {
                solved = true;
                break;
            } else {
                continue;
            }
        }

        if (solved) {
            index++;
        } else {
            board->replace(index, 0);

            try {
                backtrackIndex(index);
            }
            catch (const std::logic_error &e) {
                return false;
            }
        }
    }

    return true;
}

void Engine::clearBoard() {
    for (auto &e : *board) {
        e = 0;
    }
}

void Engine::incrementBoardIndex(int index) {
    board->replace(index, board->at(index) + 1);
}

void Engine::backtrackIndex(int &index) {
    if (isBoardIndexFilled(index - 1)) {
        if (index >= 2) {
            index -= 2;
        } else {
            throw std::logic_error("index negative");
        }
    } else {
        if (index >= 1) {
            index -= 1;
        } else {
            throw std::logic_error("index negative");
        }
    }
}

void Engine::removeRandomNumbers(int numbers) {
    for (int n = 0; n < numbers; n++) {
        board->replace(getRandomNumber(0, board->size()), 0);
    }
}

bool Engine::areSquaresCorrect() {
    for (int squareRow = 0; squareRow < SQUARE_SIZE; squareRow++) {
        for (int squareColumn = 0; squareColumn < SQUARE_SIZE; squareColumn++) {
            QVector<int> squareNumbers;
            for (int row = squareRow * SQUARE_SIZE; row < (squareRow + 1) * SQUARE_SIZE; row++) {
                for (int column = squareColumn * SQUARE_SIZE; column < (squareColumn + 1) * SQUARE_SIZE; column++) {
                    squareNumbers.append(board->at(row * SUDOKU_SIZE + column));
                }
            }
            if (not isVectorUnique(squareNumbers)) {
                return false;
            }
        }
    }

    return true;
}

bool Engine::areRowsCorrect() {
    for (int rowStart = 0; rowStart < SUDOKU_SIZE; rowStart += 1) {
        QVector<int> rowVector(board->mid(rowStart * SUDOKU_SIZE, SUDOKU_SIZE));

        if (not isVectorUnique(rowVector)) {
            return false;
        }
    }

    return true;
}

bool Engine::areColumnsCorrect() {
    for (int column = 0; column < SUDOKU_SIZE; column++) {
        QVector<int> columnVector;
        for (int row = 0; row < SUDOKU_SIZE; row++) {
            columnVector.append(board->at(row * SUDOKU_SIZE + column));
        }

        if (not isVectorUnique(columnVector)) {
            return false;
        }
    }

    return true;
}

bool Engine::isBoardIndexFilled(int index) {
    return filled->at(index);
}

int Engine::getRandomNumber(int from, int to) {
    return QRandomGenerator::global()->bounded(from, to);
}

bool Engine::isVectorUnique(QVector<int> &vector) {
    for (auto &e : vector) {
        if (e != 0 and vector.count(e) > 1) {
            return false;
        }
    }
    return true;
}
