#ifndef BOARDBUTTON_H
#define BOARDBUTTON_H

#include <QPushButton>
#include "constants.h"

class BoardButton : public QPushButton {
    int row;
    int column;
    int index;
public:
    BoardButton(int row, int column);

    int getRow() const;

    int getColumn() const;

    int getIndex() const;
};

#endif // BOARDBUTTON_H
